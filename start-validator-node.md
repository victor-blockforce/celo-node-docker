# Validator node configuration

```bash
# On the Validator machine
export CELO_IMAGE=us.gcr.io/celo-testnet/celo-node:baklava
export NETWORK_ID=62320
export VALIDATOR_NAME=blockforce
export PROXY_ENODE="" # <YOUR-PROXY-ENODE>
export PROXY_EXTERNAL_IP="" #<PROXY-MACHINE-EXTERNAL-IP-ADDRESS>
export PROXY_INTERNAL_IP="" #<PROXY-MACHINE-INTERNAL-IP-ADDRESS>
```

```bash
# On the validator machine
mkdir celo-validator-node
cd celo-validator-node
```

Creating a new account:
```bash
docker run \
    -v $PWD:/root/.celo \
    --rm -it $CELO_IMAGE account new

export CELO_VALIDATOR_SIGNER_ADDRESS="" #<YOUR-VALIDATOR-SIGNER-ADDRESS>
```

Create genesis block and run validator node:
```bash
# Reset
sudo rm -rf celo/

# Create genesis block
docker run -v $PWD:/root/.celo --rm -it $CELO_IMAGE init /celo/genesis.json

# Start validator node
docker run \
    --name celo-validator -it --restart unless-stopped \
    -p 30303:30303 \
    -p 30303:30303/udp \
    -v $PWD:/root/.celo \
    $CELO_IMAGE \
    --verbosity 3 \
    --networkid $NETWORK_ID \
    --syncmode full \
    --mine \
    --istanbul.blockperiod=5 \
    --istanbul.requesttimeout=3000 \
    --etherbase $CELO_VALIDATOR_SIGNER_ADDRESS \
    --nodiscover \
    --nousb \
    --proxy.proxied \
    --proxy.proxyenodeurlpair=enode://$PROXY_ENODE@$PROXY_INTERNAL_IP:30503\;enode://$PROXY_ENODE@$PROXY_EXTERNAL_IP:30303 \
    --unlock=$CELO_VALIDATOR_SIGNER_ADDRESS \
    --password /root/.celo/.password \
    --ethstats=$VALIDATOR_NAME@baklava-celostats-server.celo-testnet.org
```
