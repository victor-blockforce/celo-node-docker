# Set up metadata

Add relevant information about your validator and validator groups, including names, links to content, etc.

Set names:
```bash
# create a metadata object for your validator
celocli account:create-metadata ./metadata.json --from $CELO_VALIDATOR_RG_ADDRESS

# unlock accounts if necessary
celocli account:unlock $CELO_VALIDATOR_ADDRESS
celocli account:unlock $CELO_VALIDATOR_GROUP_ADDRESS
# set the name for the validator
celocli releasegold:set-account --contract $CELO_VALIDATOR_RG_ADDRESS --property name --value "mysite-v1"
celocli releasegold:set-account --contract $CELO_VALIDATOR_RG_ADDRESS --property metaURL --value "https://mysite.com"

# set the name for the validator group
celocli releasegold:set-account --contract $CELO_VALIDATOR_GROUP_RG_ADDRESS --property name --value "mysite"
celocli releasegold:set-account --contract $CELO_VALIDATOR_GROUP_RG_ADDRESS --property metaURL --value "https://mysite.com"
```